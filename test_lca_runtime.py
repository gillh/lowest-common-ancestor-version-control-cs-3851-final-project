from lcaVersionControl import get_lca
from fileNode import FileNode
import timeit
import numpy

def test_lca_times(test_cases):
	for test_case in test_cases:
		test_lca_time(test_case[0],test_case[1],test_case[2])
	
def worst_test_cases():
	return [
		(30,15,30),
		(62,31,62),
		(126,63,126),
		(510,255,510 ),
		(1022,511,1022 ),
		(4094,2047,4094 ),
		(16382, 8191,16382),
		(65534, 32767,65534),
		(131070, 131070, 65535,131070),	
		(524286, 262143, 524286),
		(1048574, 524287, 1048574)			
	]

def test_lca_time(num_childs,child_node_index_1,child_node_index_2):
	nodes_dict = create_structure(num_childs)
	
	wrapped = wrapper(get_lca,nodes_dict.get(0),nodes_dict.get(child_node_index_1),nodes_dict.get(child_node_index_2))
	t = timeit.Timer(wrapped)
	time = t.repeat(3,1)
	print num_childs, numpy.mean(time)*1000

	
def create_structure(num_nodes):
	base_node = FileNode('base.txt',None)
	nodes_dict = {}
	nodes_dict[0] = base_node
	nodes_stack = [base_node]

	nodes_added = 1;
	while(num_nodes > nodes_added):
		parent_node = nodes_stack.pop(0)
		
		child_node_1 = FileNode(nodes_added,parent_node)
		nodes_stack.append(child_node_1)
		nodes_dict[nodes_added] = child_node_1
		nodes_added += 1
		
		child_node_2 = FileNode(nodes_added,parent_node)
		nodes_stack.append(child_node_2)
		nodes_dict[nodes_added] = child_node_2
		nodes_added += 1
	return nodes_dict
	
def wrapper(func, *args, **kwargs):
    def wrapped():
        return func(*args, **kwargs)
    return wrapped
	
test_lca_times(worst_test_cases())
		
