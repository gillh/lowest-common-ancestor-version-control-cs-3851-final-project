#!/bin/bash

# SETUP 

# This is the tree that will be created with respective files
#					______________________test.txt________________________
#						/              									\
#				___test_0.txt__           						___test_1.txt____
#				/      			\								/      			\
#			test_0_0.txt       ___test_0_1.txt_____		test_1_0.txt       ___test_1_1.txt_____
#		/			\															/			\	
# test_0_0_0.txt 		test_0_0_1.txt										test_1_1_0.txt 		test_1_1_1.txt

echo SEE FILE FOR VISUAL REPRESENTATION OF GENERATED TREE


python lcaVersionControl.py add test.txt

python lcaVersionControl.py branch test.txt

python lcaVersionControl.py branch test.txt

python lcaVersionControl.py branch test_1.txt

python lcaVersionControl.py branch test_1.txt

python lcaVersionControl.py branch test_0.txt

python lcaVersionControl.py branch test_0.txt

python lcaVersionControl.py branch test_1_1.txt

python lcaVersionControl.py branch test_1_1.txt

python lcaVersionControl.py branch test_0_0.txt

python lcaVersionControl.py branch test_0_0.txt

# TEST LOWEST COMMON ANCESTOR ALGORITHM
echo
echo TEST LOWEST COMMON ANCESTOR ALGORITHM

#This test will test whether the algorithm works for two sibling files
echo TEST -- LCA -- SIBLINGS test_0.txt + test_1.txt:
EXPECTED_VALUE=test.txt
echo -e '\t'EXPECTED: $EXPECTED_VALUE 
LCA=$(python lcaVersionControl.py lca test.txt test_0.txt test_1.txt)
echo -e '\t'ACTUAL: $LCA
echo -e -n '\t'RESULT:
if [ "$LCA" == "$EXPECTED_VALUE" ];then
	echo PASS
else
	echo FAIL
fi

#This test will test whether the algorithm works for files seperated by one parent node
echo TEST -- LCA -- SEPERATED_BY_ONE_PARENT test_1_0.txt + test_1_1_1:
EXPECTED_VALUE=test_1.txt
echo -e '\t'EXPECTED: $EXPECTED_VALUE 
LCA=$(python lcaVersionControl.py lca test.txt test_1_0.txt test_1_1_1.txt)
echo -e '\t'ACTUAL: $LCA
echo -e -n '\t'RESULT:
if [ "$LCA" == "$EXPECTED_VALUE" ];then
	echo PASS
else
	echo FAIL
fi

#This test will test whether the algorithm works for files where a parent is the lowest common ancestor of its child
echo TEST -- LCA -- PARENT_IS_LCA test_0_0.txt + test_0_0_0:
EXPECTED_VALUE=test_0_0.txt
echo -e '\t'EXPECTED: $EXPECTED_VALUE 
LCA=$(python lcaVersionControl.py lca test.txt test_0_0.txt test_0_0_0.txt)
echo -e '\t'ACTUAL: $LCA
echo -e -n '\t'RESULT:
if [ "$LCA" == "$EXPECTED_VALUE" ];then
	echo PASS
else
	echo FAIL
fi

#This test will test whether the algorithm works for files that are least related -- on opposite sides of the tree
echo TEST -- LCA -- LEAST_RELATED test_0_0_0.txt + test_1_1_1:
EXPECTED_VALUE=test.txt
echo -e '\t'EXPECTED: $EXPECTED_VALUE 
LCA=$(python lcaVersionControl.py lca test.txt test_0_0_0.txt test_1_1_1.txt)
echo -e '\t'ACTUAL: $LCA
echo -e -n '\t'RESULT:
if [ "$LCA" == "$EXPECTED_VALUE" ];then
	echo PASS
else
	echo FAIL
fi


# TEST VERSION CONTROL 
echo
echo TEST VERSION CONTROL

#This test will test whether the Version Control system will merge two files with a change in one file
echo TEST -- VC -- CHANGE
FILE_1="test_1_0.txt"
FILE_2="test_1_1_1.txt"

MERGED_FILE='merged_test_1_0.txt+test_1_1_1.txt'

echo "This \nwas \na \ntest \nfile." > test_1_0.txt
echo "This \nwas \na \ntest \nfile." > expected_file.txt
echo -e '\t'FILE_1:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <$FILE_1
echo -------------------------------------
echo -e '\t'FILE_2:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <$FILE_2
echo -------------------------------------
echo -e '\t'EXPECTED_MERGED_FILE:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <expected_file.txt
echo -------------------------------------

python lcaVersionControl.py merge test_1_0.txt test_1_1_1.txt

echo -e '\t'ACTUAL_MERGED_FILE:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <$MERGED_FILE
echo -------------------------------------

echo -n RESULT: 
if diff "expected_file.txt" "$MERGED_FILE" >/dev/null ; then
  echo PASS
else
  echo FAIL
fi

#This test will test whether the Version Control system will merge two files with an addition in one file
echo TEST -- VC --  ADDITION
FILE_1="test_1_0.txt"
FILE_2="test_1_1_1.txt"

MERGED_FILE='merged_test_1_0.txt+test_1_1_1.txt'

echo "This \nwas \na \nadd \ntest \nfile." > test_1_0.txt
echo "This \nwas \na \nadd \ntest \nfile." > expected_file.txt
echo -e '\t'FILE_1:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <$FILE_1
echo -------------------------------------
echo -e '\t'FILE_2:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <$FILE_2
echo -------------------------------------
echo -e '\t'EXPECTED_MERGED_FILE:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <expected_file.txt
echo -------------------------------------

python lcaVersionControl.py merge test_1_0.txt test_1_1_1.txt

echo -e '\t'ACTUAL_MERGED_FILE:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <$MERGED_FILE
echo -------------------------------------

echo -n RESULT: 
if diff "expected_file.txt" "$MERGED_FILE" >/dev/null ; then
  echo PASS
else
  echo FAIL
fi

#This test will test whether the Version Control system will merge two files with an deletion in one file
echo TEST -- VC --  DELETION
FILE_1="test_1_0.txt"
FILE_2="test_1_1_1.txt"

MERGED_FILE='merged_test_1_0.txt+test_1_1_1.txt'

echo "This \nwas \ntest \nfile." > test_1_0.txt
echo "This \nwas \ntest \nfile." > expected_file.txt
echo -e '\t'FILE_1:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <$FILE_1
echo -------------------------------------
echo -e '\t'FILE_2:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <$FILE_2
echo -------------------------------------
echo -e '\t'EXPECTED_MERGED_FILE:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <expected_file.txt
echo -------------------------------------

python lcaVersionControl.py merge test_1_0.txt test_1_1_1.txt

echo -e '\t'ACTUAL_MERGED_FILE:
echo -------------------------------------
	while IFS= read -r LINE
	do
		echo -e $LINE
	done <$MERGED_FILE
echo -------------------------------------

echo -n RESULT: 
if diff "expected_file.txt" "$MERGED_FILE" >/dev/null ; then
  echo PASS
else
  echo FAIL
fi


# TEAR DOWN

# remove branch_files, merged_files, expected files, and pickle file

rm test_0*
rm test_1*
rm merged*
rm expected*
rm files.p
	
