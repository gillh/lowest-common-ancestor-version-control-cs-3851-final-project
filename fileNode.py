import os.path as p

class FileNode:
	def __init__(self, filename, ancestor):
		if(ancestor is not None):
			if(ancestor.numChildren < 2):
				ancestor.numChildren += 1
				if not ancestor.left:
					ancestor.left = self
				else:
					ancestor.right = self
		self.left = None
		self.right = None
		self.ancestor = ancestor
		self.filename = filename
		self.numChildren = 0;
		
	def getFile(self):
		return self.filename
		
		
