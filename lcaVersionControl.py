import click
import pickle
import os
from shutil import copyfile
from fileNode import FileNode

PICKLE_FILENAME = 'files.p'		

#  
#  	The Main Method
#	Executes the appropriate command	
#  
@click.group()
def main():
	pass

#	Function used to determin if file exists
#	@param filename -- name of file that is to be checked
def file_exists(filename):
	if(os.path.exists(filename) is False):
		print "ERROR: FILE NOT FOUND"
		return False
	return True

# Add command
# Adds the corresponding file to the system and creates a structure for it
@click.command('add (filename) -- add the corresponding file to the system')
@click.argument('filename')
def add(filename):
	if(not file_exists(filename)):
		return
	pickle_file = open(PICKLE_FILENAME,'wb')
	pickle.dump({filename: FileNode(filename,None)},pickle_file)
	pickle_file.close()
	
# Branch command
# Branches a file off of corresponding file and copy contents to it	
@click.command('branch (filename) -- branches a file off of corresponding file')
@click.argument('filename')
def branch(filename):
	try:
		pickle_file = open(PICKLE_FILENAME,'rb')
	except IOError:
		print "ERROR: NO FILES ADDED.  ADD A FILE TO BRANCH"
		return
	file_dict = pickle.load(pickle_file)
	pickle_file.close()
	parent_file_node = file_dict.get(filename)
	if(parent_file_node is None):
		print "ERROR: FILE NOT ADDED"
		return
	if(parent_file_node.numChildren >= 2):
		print "ERROR: CANNOT ADD ANOTHER CHILD TO THIS NODE"
		return
	child_filename = filename[:-4] + '_' + str(parent_file_node.numChildren) + filename[-4:] 		# generate file name of new child file_name -- add _0 if left child and _1 if right child
	copyfile(parent_file_node.filename,child_filename)
	file_dict[child_filename] = FileNode(child_filename, parent_file_node)
	pickle_file = open(PICKLE_FILENAME,'wb')
	pickle.dump(file_dict,pickle_file)
	pickle_file.close()
	
# SeeAncestors command
# Prints out all of the ancestors of the corresponding file
@click.command('seeancestors (filename) -- prints the filename of all ancestors of this file')
@click.argument('filename')
def seeAncestors(filename):
	pickle_file = open(PICKLE_FILENAME,'rb')
	file_dict = pickle.load(pickle_file)
	file_node = file_dict.get(filename)
	if(file_node is None):
		print "ERROR: FILE NOT FOUND"
	else:
		cont = True
		while(cont):
			print file_node.filename
			if(file_node.ancestor is not None):
				file_node = file_node.ancestor
			else:
				cont = False

# Function returns the root of the tree in which two file nodes exist
# @param file_node_1 -- file_node
# @param file_node_2 -- file_node
# @return root_node
def get_root(file_node_1,file_node_2):
	root_file_1 = file_node_1
	while True:
		if(root_file_1.ancestor is not None):
			root_file_1 = root_file_1.ancestor
		else:
			break
	root_file_2 = file_node_2
	while True:
		if(root_file_2.ancestor is not None):
			root_file_2 = root_file_2.ancestor
		else:
			break
	if(root_file_1 is root_file_2):
		return root_file_1
	else:
		return None
			
# Function returns the Lowest Common Ancestor of two file nodes
# @param root_node -- root file node of tree
# @param file_node_1 -- file_node
# @param file_node_2 -- file_node
# @return LCA node
def get_lca(root_node,file_node_1, file_node_2):
	if(root_node is None):
		return None
	if(root_node is file_node_1 or root_node is file_node_2):
		return root_node
	left = get_lca(root_node.left, file_node_1, file_node_2)
	right = get_lca(root_node.right, file_node_1, file_node_2)
	if(left and right):
		return root_node
	if(left):
		return left
	else:
		return right

# LCA command
# Prints out the lowest common ancestor of two files
@click.command('lca (filename_1) (filename_2) -- prints the Lowest Common Ancestor of the two files')
@click.argument('root_filename')
@click.argument('filename_1')
@click.argument('filename_2')
def lca(root_filename,filename_1,filename_2):
	pickle_file = open(PICKLE_FILENAME,'rb')
	file_dict = pickle.load(pickle_file)
	
	root_node = file_dict.get(root_filename)
	file_node_1 = file_dict.get(filename_1)
	file_node_2 = file_dict.get(filename_2)
	
	lca = get_lca(root_node,file_node_1,file_node_2).filename
	print lca
	return lca
	
	
# Function that will poll an line from a queue of file lines
# @param file_lines -- queue to poll
# @return line or empty string if all lines have been removed
def pop_line(file_lines):
	try:
		return file_lines.pop(0)
	except IndexError:
		return ""

# Function to handle a merge conflict situation. 
# Displays prompt to user to choose a line to keep in the merged file
# @param file_line_1 -- line from file 1
# @param file_line_2 -- line from file 2
# @return file_line that is selected
def handle_conflict(file_1_line,file_2_line):
	selection = 0
	while selection != 1 or selection != 2:
		selection = input('MERGE CONFLICT: \n1: %s\n2: %s' % (file_1_line,file_2_line))
		if(selection == 1):
			return file_1_line
		elif (selection == 2):
			return file_2_line
		else:
			print "\nINVALID CHOICE"

# Function that handles the merging of two files
# Generates a merged file
# @param lca_node -- lowest common ancestor file node
# @param file_node_1 -- file node
# @param file_node_2 -- file node
def merge_files(lca_node,file_node_1,file_node_2):
	lca_lines = open(lca_node.filename).readlines()
	file_1_lines = open(file_node_1.filename).readlines()
	file_2_lines = open(file_node_2.filename).readlines()
	merged_lines = []
	
	while file_1_lines or file_2_lines:
		file_1_line = pop_line(file_1_lines)
		file_2_line = pop_line(file_2_lines)
		lca_line = pop_line(lca_lines)
		if(file_1_line == file_2_line):
			merged_lines.append(file_1_line)
		elif(file_1_line != lca_line and file_2_line == lca_line):
			merged_lines.append(file_1_line)
		elif(file_1_line == lca_line and file_2_line != lca_line):
			merged_lines.append(file_2_line)
		else:
			merged_lines.append(handle_conflict(file_1_line,file_2_line))
	merge_file = open('merged_%s+%s'%(file_node_1.filename,file_node_2.filename),'wb')
	merge_file.write("".join(map(lambda x: str(x), merged_lines)))
	merge_file.close()
				
# Merge Command
# Merges two files together using Three Way Merge
# Generate a new merged file
@click.command('merge (filename_1) (filename_2) -- merge two files using 3 Way Merge with Lowest Common Ancestor')
@click.argument('filename_1')
@click.argument('filename_2')
def merge(filename_1, filename_2):
	pickle_file = open(PICKLE_FILENAME,'rb')
	file_dict = pickle.load(pickle_file)
	
	file_node_1 = file_dict.get(filename_1)
	file_node_2 = file_dict.get(filename_2)
	root_node = get_root(file_node_1, file_node_2)
	
	lca_node = get_lca(root_node,file_node_1,file_node_2)
	return merge_files(lca_node,file_node_1,file_node_2)
	
main.add_command(add)
main.add_command(branch)
main.add_command(seeAncestors)
main.add_command(merge)
main.add_command(lca)

if __name__ == "__main__":
   main()
